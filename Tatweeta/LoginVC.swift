//
//  LoginVC.swift
//  Tatweeta
//
//  Created by yo7ia on 2/19/17.
//  Copyright © 2017 Mohamed Elmakkawy. All rights reserved.
//

import UIKit
import Accounts
import Social
import SafariServices
import SwifteriOS
class LoginVC :UIViewController, SFSafariViewControllerDelegate
{
    var swifter: Swifter
    
    // Default to using the iOS account framework for handling twitter auth
    let useACAccount = false
    
    required init?(coder aDecoder: NSCoder) {
        self.swifter = Swifter(consumerKey: "JmHLR8OlJko0lsLANqevBSE4U", consumerSecret: "a98jClXzB2QhcbfgZlZgWT30Dng41WHq0ByY1ODjNANbgZFT9C")
        super.init(coder: aDecoder)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }
    
    
    @IBAction func Login() {
        let failureHandler: (Error) -> Void = { error in
            print(error.localizedDescription)
            
        }
        
        if useACAccount {
            let accountStore = ACAccountStore()
            let accountType = accountStore.accountType(withAccountTypeIdentifier: ACAccountTypeIdentifierTwitter)
            
            // Prompt the user for permission to their twitter account stored in the phone's settings
            accountStore.requestAccessToAccounts(with: accountType, options: nil) { granted, error in
                guard granted else {
                   print(error!.localizedDescription)
                    return
                }
                
                let twitterAccounts = accountStore.accounts(with: accountType)!
                
                if twitterAccounts.isEmpty {
                   print("There are no Twitter accounts configured. You can add or create a Twitter account in Settings.")
                } else {
                    let twitterAccount = twitterAccounts[0] as! ACAccount
                    self.swifter = Swifter(account: twitterAccount)
                    
//                    self.swifter.client.credential
//                    self.navigationController?.PushVC(identifer: "rootVc2", animated: true)
                    self.performSegue(withIdentifier: "gotorootvc2", sender: nil)

//                    self.fetchTwitterHomeStream()
                }
            }
        } else {
            let url = URL(string: "swifter://success")!
            
            swifter.authorize(with: url, presentFrom: self, success: { t in
//                self.fetchTwitterHomeStream()
                UIView.animate(withDuration: 0.3, animations: {
                    //self.performSegue(withIdentifier: "gotorootvc2", sender: nil)

                  
                    let window = (UIApplication.shared.delegate as? AppDelegate)?.window
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "rootVc2")
                     window?.rootViewController = vc
                    
                })
//                self.navigationController?.PushVC(identifer: "rootVc2", animated: true)
               print((t.0?.userID)!+" tttt ")
            }, failure: failureHandler)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotorootvc2" {
        // let vc = segue
        }
    }
    
    
    @available(iOS 9.0, *)
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
