//
//  galleryVc.swift
//  School Communicator
//
//  Created by Mohamed Elmakkawy on 1/23/17.
//  Copyright © 2017 Mohamed Elmakkawy. All rights reserved.
//

import UIKit
import CSStickyHeaderFlowLayout



class collectionContainer  {
    
     static func handelLayout(controller:UIViewController,container:UIView)->UIViewController{
    
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        container.addSubview((controller.view)!)
        
        if #available(iOS 9.0, *) {
            NSLayoutConstraint.activate([
                controller.view.leadingAnchor.constraint(equalTo: container.leadingAnchor),
                controller.view.trailingAnchor.constraint(equalTo:container.trailingAnchor),
                controller.view.topAnchor.constraint(equalTo:container.topAnchor),
                controller.view.bottomAnchor.constraint(equalTo:container.bottomAnchor)
                ])
        } else {
            // Fallback on earlier versions
            
            
            
            let leadingConstraint = NSLayoutConstraint(item: controller.view, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1, constant: 0)
            
            let trailingConstraint = NSLayoutConstraint(item: controller.view, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1, constant: 0)
            
            let topConstraint = NSLayoutConstraint(item: controller.view, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1, constant: 0)
            
            let bottomConstraint = NSLayoutConstraint(item: controller.view, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1, constant: 0)
            
            
            
            
            NSLayoutConstraint.activate([
                leadingConstraint,
                trailingConstraint,
                topConstraint,
                bottomConstraint
                
                ])
        }
        
    return controller
    
    }

    
}


class MainCollectionVc: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var headerHight:CGFloat!=0.0
    var followers : [follower]!
    
    
    var layout : CSStickyHeaderFlowLayout? {
        return self.collectionView?.collectionViewLayout as? CSStickyHeaderFlowLayout
    }
    
//    var type :String!
//    var dataSource:[String]!
//    var titles:[String]!
//    var details:[String]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerHight =  self.collectionView.frame.size.height/5
        
//        if #available(iOS 10.0, *) {
        if #available(iOS 10.0, *) {
            self.collectionView?.isPrefetchingEnabled = false
        } else {
            // Fallback on earlier versions
        }
    
        
        let myNib = UINib(nibName: "CSGrowHeader",bundle: nil)
        self.collectionView!.register(myNib, forSupplementaryViewOfKind:UICollectionElementKindSectionHeader, withReuseIdentifier: "sectionHeader")
        
        self.collectionView.register(UINib(nibName: "mainHeaderCell",bundle: nil), forCellWithReuseIdentifier: "mainHeaderCell")
        
         self.collectionView?.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "mainHeaderCell")
        
        self.collectionView?.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "sectionHeader")
        self.layout!.headerReferenceSize = CGSize(width: self.collectionView!.frame.size.width, height: 0)
        
        self.collectionView.reloadData()
        
        
        //delete header Notification
        NotificationCenter.default.addObserver(self, selector: #selector(MainCollectionVc.deleteheader(notification:)), name: NSNotification.Name(rawValue: "deleteheader"), object: nil)
        //delete Cell Notification-->by index
        NotificationCenter.default.addObserver(self, selector: #selector(MainCollectionVc.deleteCell(notification:)), name: NSNotification.Name(rawValue: "deleteCell"), object: nil)
        
    }

    func deleteCell(notification:NSNotification)
    {
     let cellWillRemoved = notification.userInfo?["index"] as! IndexPath
        
        print("index : \(cellWillRemoved.row)")
        
    }
    
    func deleteheader(notification:NSNotification)
    {
        headerHight = 0.0
        self.collectionView.reloadData()
    
    }
    

}


extension MainCollectionVc : UICollectionViewDataSource  {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return followers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
    
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! mainCollectionCell
        
        cell.name.text = followers[indexPath.row].name
        cell.cellTag.text = followers[indexPath.row].tag
        cell.tweets.text = followers[indexPath.row].tweets
        cell.followers.text = followers[indexPath.row].followers
        cell.following.text = followers[indexPath.row].following
        
        if followers[indexPath.row].type == .fans||followers[indexPath.row].type == .recentFollower||followers[indexPath.row].type == .blackList||followers[indexPath.row].type == .nearbyFollow {
            cell.mainBt.setImage(UIImage(named:"follow"), for: .normal)
        }
        
        
        cell.configrationCell(index: indexPath)
        
            return cell
        
        
    }
    
}


extension MainCollectionVc :UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        if type == "Group"{
//            let chatViewVc = self.storyboard?.instantiateViewController(withIdentifier: "chatView") as! chatView
//           var dataSource: FakeDataSource!
//            dataSource = FakeDataSource(count: 0, pageSize: 50)
//            chatViewVc.dataSource = dataSource
//            chatViewVc.messageSender = dataSource.messageSender
//            chatViewVc.title = titles [indexPath.row]
//            self.navigationController?.show(chatViewVc, sender: self)
//            
//        }else{
//            
//            let masterAlbum = self.storyboard?.instantiateViewController(withIdentifier: "masterAlbumsVc")
//            self.navigationController?.show(masterAlbum!, sender: self)
//        
//        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.size.width/2, height: ((collectionView.bounds.size.width/2-10)/3)*2+30)
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
      return  UIEdgeInsetsMake(0,0,0,0)
    }
  
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: self.collectionView.frame.size.width, height: headerHight)
    
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
     
            let cell = self.collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "sectionHeader", for: indexPath)
            for v in cell.subviews {
                v.removeFromSuperview()
            }
        
        let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "mainHeaderCell", for: indexPath) as! mainHeaderCell
        
        cell2.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: self.collectionView.frame.size.height/5)
        
        cell.addSubview(cell2)
      
       
            return cell
        
    }

}

