//
//  Validation.swift
//  School Communicator
//
//  Created by Mohamed Elmakkawy on 1/31/17.
//  Copyright © 2017 Mohamed Elmakkawy. All rights reserved.
//

import Foundation


class Validation {
    enum ValidationOption {
        case Defailt
        case Email
    }
    
    
   static func ValidateFaild(text:String)-> Bool {
        if text == "" {
            return false
        }else {
            return true
        }
        
    }
    
   static func isValidEmail(emailText:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailText)
    }
    
    
    static func ValidationTextFails(Text:String,type:ValidationOption)->Bool  {
        
        switch type {
        case .Defailt: return ValidateFaild(text: Text)
        case .Email: return isValidEmail(emailText: Text)
        }
        
        
        
        
        
    }
    
}
