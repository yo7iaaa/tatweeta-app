//
//  extentions.swift
//  School Communicator
//
//  Created by Mohamed Elmakkawy on 1/19/17.
//  Copyright © 2017 Mohamed Elmakkawy. All rights reserved.
//

import UIKit


extension String {
    
    func localized() -> String {
        
        var path:String!=""
        if  Bundle.main.preferredLocalizations.first == "ar" {
            
            path = Bundle.main.path(forResource: "ar", ofType: "lproj")
        }else{
            path = Bundle.main.path(forResource: "en", ofType: "lproj")
        }
        
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        
    }
    
}


extension UINavigationController
{
     func PushVC(identifer: String,animated: Bool?) -> Void
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: identifer)
        self.show(vc,sender: nil)
        
    }
}

extension UIImage {
    static func setImgeByOrintation(named:String)->UIImage  {
        var image :UIImage = UIImage(named: named)!
        if  Bundle.main.preferredLocalizations.first == "ar" {
            image = UIImage(cgImage: (image.cgImage)!, scale: 1.0, orientation: .upMirrored)
        }
        return image
    }
    
}



extension UIFont {
   
    static var tabBarSize:CGFloat!=10.0
    
    static func setBoldFont(size:CGFloat)->UIFont{
        if  Bundle.main.preferredLocalizations.first == "ar" {
            
            return UIFont(name: "GE SS Two", size: size)!
            
        }else {
            return UIFont(name: "Odin-Bold", size: size)!
        }
        
    }
    
    static func setLightFont(size:CGFloat)->UIFont{
        if  Bundle.main.preferredLocalizations.first == "ar" {
            return UIFont(name: "GE SS", size: size)!
        }else {
            return UIFont(name: "OdinRounded-Light", size: size)!
        }
        
    }
    
    
    static func setBarsFont() {
        if  Bundle.main.preferredLocalizations.first == "ar" {
            //set tab bar font in ar
            //UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont(name: "GE SS", size: UIFont.tabBarSize)!], for: .normal)
            //set navigation bar font in ar
            //UINavigationBar.appearance().titleTextAttributes = [
               // NSFontAttributeName: UIFont(name: "GE SS", size: 20)!
           // ]
            
        }else {
            //set tab bar font in en
            //UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont(name: "OdinRounded-Light", size: UIFont.tabBarSize)!], for: .normal)
            //set navigation bar font in en
            //UINavigationBar.appearance().titleTextAttributes = [
              //  NSFontAttributeName: UIFont(name: "OdinRounded-Light", size: 20)!
           // ]
            
        }
    }
    
}



