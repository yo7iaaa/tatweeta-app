//
//  follower.swift
//  Tatweeta
//
//  Created by Mohamed Elmakkawy on 2/18/17.
//  Copyright © 2017 Mohamed Elmakkawy. All rights reserved.
//

import Foundation

class follower {
    
    enum followerType {
        
        case nonFollowers
        case fans
        case recentunFollower
        case recentFollower
        case inactiveFollower
        case allFollowing
        case nearbyFollow
        case whiteList
        case blackList
        
    }
    
    var name:String!
    var tag:String!
    var tweets:String!
    var followers:String!
    var following:String!
    var isDeleted:Bool!
    var type:followerType!
  
    
    
}
