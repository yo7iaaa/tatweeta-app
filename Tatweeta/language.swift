//
//  language.swift
//  Tatweeta
//
//  Created by Mohamed Elmakkawy on 2/15/17.
//  Copyright © 2017 Mohamed Elmakkawy. All rights reserved.
//

import UIKit

class language {
    
    class func currentLang () -> String {
      let def = UserDefaults.standard
      let langs = def.object(forKey: "AppleLanguages") as! NSArray
      let firstLang=langs.firstObject as! String
      return firstLang
    }

    class func setLanguage(lang:String){
     let def = UserDefaults.standard
     def.set([lang], forKey: "AppleLanguages")
     def.synchronize()
        
     refreshWindow()
    }
    
    private class func refreshWindow()  {
        if language.currentLang() == "ar"{
            if #available(iOS 9.0, *) {
                
                UIView.appearance().semanticContentAttribute = .forceRightToLeft
            } else {
                // Fallback on earlier versions
            }
        }else {
            if #available(iOS 9.0, *) {
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
            } else {
                // Fallback on earlier versions
            }
        }
        let window = (UIApplication.shared.delegate as? AppDelegate)?.window
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        window?.rootViewController = storyBoard.instantiateViewController(withIdentifier: "rootVc")
        UIView.transition(with: window!, duration: 0.5, options: .transitionFlipFromLeft, animations: nil, completion: nil)
    }
    
}

class localizer {
    class func doExchange(){
        ExchangeMethodForeClass(className: Bundle.self, originalSelector:#selector(Bundle.localizedString(forKey:value:table:)), overrideSelector: #selector(Bundle.customLocalizedString(forKey:value:table:)))
        
        ExchangeMethodForeClass(className: UIApplication.self, originalSelector: #selector(getter: UIApplication.userInterfaceLayoutDirection), overrideSelector: #selector(getter: UIApplication.custom_userInterFaceLayoutDirection))
    }

}


extension Bundle {
    func customLocalizedString(forKey key: String, value: String?, table tableName: String?) -> String{
    
        let current = language.currentLang()
    var bundle = Bundle()
        if let path = Bundle.main.path(forResource: current, ofType: "lproj"){
            
            print("path:",path)
            bundle = Bundle(path: path)!
        }else {
            let path2 = Bundle.main.path(forResource: "Base", ofType: "lproj")
            bundle = Bundle(path: path2!)!
        }
        
        return bundle.customLocalizedString(forKey: key, value: value, table: tableName)
        
    }

}


extension UIApplication{
    
    var custom_userInterFaceLayoutDirection: UIUserInterfaceLayoutDirection {
     get{
        
        var direction = UIUserInterfaceLayoutDirection.leftToRight
        if language.currentLang() == "ar" {
            direction = .rightToLeft
        }
        return direction
        
     }
    }
    

}

func ExchangeMethodForeClass (className:AnyClass,originalSelector:Selector ,overrideSelector:Selector){

    let originalMethod :Method = class_getInstanceMethod(className, originalSelector)
    let overrideMethod :Method = class_getInstanceMethod(className, overrideSelector)
    
    
    if class_addMethod(className, originalSelector, method_getImplementation(overrideMethod), method_getTypeEncoding(overrideMethod)){
    
        class_replaceMethod(className, overrideSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod))
    }else {
        method_exchangeImplementations(originalMethod, overrideMethod)
    }
}
