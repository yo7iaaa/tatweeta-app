//
//  mainCollectionCell.swift
//  Tatweeta
//
//  Created by Mohamed Elmakkawy on 2/18/17.
//  Copyright © 2017 Mohamed Elmakkawy. All rights reserved.
//

import UIKit

class mainCollectionCell: UICollectionViewCell {
    
    var cellIndex : IndexPath!
    
    @IBOutlet var bgView: UIView!
    @IBOutlet var replyBt: UIButton!
    @IBOutlet var replyBg: UIView!
    @IBOutlet var blaclListbt: UIButton!
    @IBOutlet var menuBt: UIButton!
    @IBOutlet var mainBt: UIButton!
    
    //===================
    
    
    @IBOutlet var tweets: UILabel!
    @IBOutlet var followers: UILabel!
    @IBOutlet var following: UILabel!
    @IBOutlet var name: UILabel!
    @IBOutlet var cellTag: UILabel!
    
    
    
    @IBAction func blaclListAction(_ sender: UIButton) {
         replyBg.isHidden = true
    }
    
    
    @IBAction func replyAction(_ sender: UIButton) {
         replyBg.isHidden = true
    }
    
    
    @IBAction func menuAction(_ sender: UIButton) {
        
        replyBg.isHidden = false
        
    }
    
    @IBAction func mainAction(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deleteCell"), object: nil,userInfo: ["index":cellIndex])
        
      
        
        
    }
    
    func configrationCell (index:IndexPath) {
        cellIndex = index
    }
    
}
