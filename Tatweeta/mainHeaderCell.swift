//
//  mainHeaderCell.swift
//  Tatweeta
//
//  Created by Mohamed Elmakkawy on 2/18/17.
//  Copyright © 2017 Mohamed Elmakkawy. All rights reserved.
//

import UIKit

class mainHeaderCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func removeHeaderAction(_ sender: UIButton) {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deleteheader"), object: nil)
    }
}
