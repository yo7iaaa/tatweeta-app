//
//  menuTable.swift
//  Tatweeta
//
//  Created by Mohamed Elmakkawy on 2/15/17.
//  Copyright © 2017 Mohamed Elmakkawy. All rights reserved.
//

import UIKit

class menuTable: UITableViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.row == 13 {
            let alert = UIAlertController(title: "Choose Lang", message: "", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Arabic", style: .default, handler: { action in
            
                self.changeLangue(lang: "ar")
                
            }))
            alert.addAction(UIAlertAction(title: "English", style: .default, handler: { action in
               self.changeLangue(lang: "en")
                
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            
            self.present(alert, animated: true, completion: nil)
           
        }
        
    }

    func changeLangue(lang:String) {
        
        let alert = UIAlertController(title: "langMsg", message: nil, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: {action in
            UserDefaults.standard.set([lang], forKey: "AppleLanguages")
            UserDefaults.standard.synchronize()
            
            exit(0)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 70))
        view.backgroundColor = UIColor.red
        return view
    }
   
   

}
